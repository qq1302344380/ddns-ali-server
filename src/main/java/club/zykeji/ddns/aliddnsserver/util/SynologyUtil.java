package club.zykeji.ddns.aliddnsserver.util;

import club.zykeji.ddns.aliddnsserver.common.Result;
import cn.hutool.core.util.StrUtil;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.alidns.model.v20150109.DescribeSubDomainRecordsRequest;
import com.aliyuncs.alidns.model.v20150109.DescribeSubDomainRecordsResponse;
import com.aliyuncs.alidns.model.v20150109.UpdateDomainRecordRequest;
import com.aliyuncs.alidns.model.v20150109.UpdateDomainRecordResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;

import java.util.List;

/**
 * Create by 丶TheEnd on 2021/1/14.
 * @author TheEnd
 */
public class SynologyUtil {

    private static final String REGION_ID = "cn-hangzhou";

    public static DefaultProfile getDefaultProfile(String accessKeyId, String secret) {
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, accessKeyId, secret);
        return profile;
    }

    public static IAcsClient getIAcsClient(DefaultProfile defaultProfile) {
        return new DefaultAcsClient(defaultProfile);
    }

    public static IAcsClient getIAcsClient(String accessKeyId, String secret) {
        DefaultProfile profile = DefaultProfile.getProfile(REGION_ID, accessKeyId, secret);
        return new DefaultAcsClient(profile);
    }

    /**
     * 执行更新
     * @param host
     */
    public static Result update(String accessKeyId, String secret, String host, String myIp) {
        IAcsClient client = getIAcsClient(accessKeyId, secret);
        DescribeSubDomainRecordsResponse.Record record = getRecodesByHost(client, host, "A");
        if (record == null) {
            return Result.error("当前DNS不存在");
        }
        String secondaryDomain = StringUtil.getSecondaryDomain(host);
        UpdateDomainRecordRequest request = new UpdateDomainRecordRequest();
        request.setRecordId(record.getRecordId());
        request.setRR(secondaryDomain);
        request.setType("A");
        request.setValue(myIp);

        try {
            UpdateDomainRecordResponse response = client.getAcsResponse(request);
            return Result.success();
        } catch (ServerException e) {
            e.printStackTrace();
            return Result.error();
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
            return Result.error(e.getErrMsg());
        }

    }

    /**
     * 获取域名解析详细信息
     * @param host
     * @param type
     * @return
     */
    public static DescribeSubDomainRecordsResponse.Record getRecodesByHost(IAcsClient client, String host, String type) {
        DescribeSubDomainRecordsRequest request = new DescribeSubDomainRecordsRequest();
        request.setSubDomain(host);

        DescribeSubDomainRecordsResponse response;

        try {
            response = client.getAcsResponse(request);
            List<DescribeSubDomainRecordsResponse.Record> domainRecords = response.getDomainRecords();
            for (DescribeSubDomainRecordsResponse.Record domainRecord : domainRecords) {
                if (type.toUpperCase().equals(domainRecord.getType())) {
                    return domainRecord;
                }
            }
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
        }

        return null;
    }

}
