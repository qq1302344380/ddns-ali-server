package club.zykeji.ddns.aliddnsserver.controller;

import club.zykeji.ddns.aliddnsserver.common.Result;
import club.zykeji.ddns.aliddnsserver.util.SynologyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by 丶TheEnd on 2021/1/14.
 * @author TheEnd
 */
@RestController
@RequestMapping("/synology")
@Slf4j
public class SynologyController {

    @GetMapping("/update")
    public Result update(@RequestParam String hostName,
                         @RequestParam String myIp,
                         @RequestParam String userName,
                         @RequestParam String password) {
        log.info("接收到解析请求: {} -> {} :: {} -> {}", userName, password, hostName, myIp);
        Result result = SynologyUtil.update(userName, password, hostName, myIp);
        if (Result.success().equals(result)) {
            log.info("解析修改成功: {} -> {}", hostName, myIp);
        }
        return result;
    }

}
