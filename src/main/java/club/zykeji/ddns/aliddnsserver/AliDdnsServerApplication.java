package club.zykeji.ddns.aliddnsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author TheEnd
 */
@SpringBootApplication
public class AliDdnsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliDdnsServerApplication.class, args);
    }

}
