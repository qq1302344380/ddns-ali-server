package club.zykeji.ddns.aliddnsserver;

import club.zykeji.ddns.aliddnsserver.util.DnsUtil;
import org.junit.jupiter.api.Test;

/**
 * Create by 丶TheEnd on 2020/5/5.
 */
public class DnsTest {

    @Test
    public void testGetIp() {
        System.out.println(new DnsUtil().getIpByHost("ddns.zykeji.club"));
    }

    @Test
    public void stringTest() {
        String host = "as.dasge.www.www.baidu.com";
        int lastDelimiter = host.lastIndexOf(".", host.lastIndexOf(".") - 1);
        System.out.println(host.substring(0, lastDelimiter));
    }

}
