
## ZYDDNS 一款简单便携,方便快速部署的DDNS服务器 
    
     
如果对您有帮助，您可以点右上角 "Star" 支持一下，这样才有继续维护下去的动力，谢谢！

-  目前仅支持阿里云DNS(域名需要在阿里云平台注册购买) 

### 软件架构

- SpringBoot
- 阿里云DNS

### 项目简述:

本项目为DDNS Server端,采用Java语言开发,可以运行在任意支持JDK的平台上;  
服务端对外提供HTTP接口供客户端调用,可以根据个人喜好编写任意变成语言,运行在任意操作系统的客户端,只要使用的编程语言可以发送HTTP请求;  
目前仅支持阿里云购买的域名,后续会添加其他云服务提供厂商的支持;  

### 客户端:  
客户端可以自己开发,或者使用已经做好的Java客户端  
客户端地址: [Java客户端](https://gitee.com/qq1302344380/zyddns-client-java)  

### 部署教程(需先安装jdk, maven, git):

##### 将项目clone到本地:
``` shell
git clone https://gitee.com/qq1302344380/ddns-ali-server.git
```

##### 进入项目根目录
``` shell
cd ddns-ali-server
```

##### 修改配置文件:
``` shell
vim ./src/main/resources/application.properties

server.port=要监听的端口号

ali.profile.regionId=cn-hangzhou
ali.profile.accessKeyId=阿里云申请的accessKeyId
ali.profile.secret=阿里云申请的secret

```


##### 使用maven进行打包:
``` shell
mvn clean package
```

##### 运行服务端:
``` shell
java -jar ddns-ali-server-0.0.1-SNAPSHOT.jar

##### ### 后台方式运行
nohup java -jar ddns-ali-server-0.0.1-SNAPSHOT.jar &
```


### HTTP 接口:
- 获取本机公网IP  
请求URL: /d-dns/ip/my  
请求方式: GET  
参数: 无  
响应结果示例: 
``` json
{code: 200, msg: "220.181.38.150"}
```

- 获取本机IP字符串  
  _返回值只包含字符串,不需要解析JSON,但是不好判断服务器请求是否成功(无状态码)_   
请求URL: /d-dns/ip/my/string  
请求方式: GET  
参数: 无  
响应结果示例: 
``` json
220.181.38.150
```

- 获取域名解析的IP  
请求URL: /d-dns/dns/bind/current  
请求方式: GET  
参数:   

参数名|类型|示例|说明
---|---|---|---
host|String|www.baidu.com|要查询的域名地址


响应结果示例: 
``` json
{"code":200,"msg":"220.181.38.150"}
```

- 绑定域名和IP  
请求URL: /d-dns/dns/bind  
请求方式: GET  
参数:

参数名|类型|示例|说明
---|---|---|---
host|String|www.baidu.com|要绑定的域名地址
value|String|220.181.38.150|要绑定的IP地址

### 客户端开发思路:
 **定时任务请求服务端获取当前的IP和当前域名已绑定的IP,判断IP如果发生变化,发送请求更新域名解析** 

#### 使用说明

如果有什么建议或在使用过程中出现什么问题,欢迎提交Issues或在下方评论

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
